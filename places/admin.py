from django.contrib import admin
from places.models import Place
from django.contrib.admin import AdminSite

AdminSite.site_title = 'My Admin'

AdminSite.site_header = 'Meeting Planner'

AdminSite.index_title = ''


class PlaceAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'floor', 'phone')
        }),
        ('Advanced options', {
            'classes': ('collapse',),
            'fields': ('exchange_sync', 'mail', 'exchange_id'),
        }),
    )
    list_display = ('name', 'floor', 'mail', 'phone')
    list_filter = ('floor', 'exchange_sync')


# Register your models here.
admin.site.register(Place, PlaceAdmin)
