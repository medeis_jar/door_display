from django.db import models
# from django.db.models.signals import post_save
# from django.dispatch import receiver
# import sys


# Create your models here.

class Place(models.Model):

    name = models.CharField(max_length=100)
    floor = models.DecimalField(max_digits=3, decimal_places=0)
    phone = models.CharField(max_length=100, null=True, blank=True)
    exchange_sync = models.BooleanField(default=False)
    mail = models.EmailField(null=True, blank=True)
    exchange_id = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = 'Miejsce'
        verbose_name_plural = 'Miejsca'

    # Display name of object in object list view
    def __str__(self):
        return self.name

    # Adds view on site button in admin change object
    def get_absolute_url(self):
        return "/places/%i/" % self.id

      
# In the past for saving full url of place
# @receiver(post_save, sender=Place)
# def update_url(sender, instance, created, **kwargs):
#     # without this check the save() below causes infinite post_save signals
#     if created:
#
#         obj = Place.objects.get(id=instance.id)
#         instance.url = obj.get_absolute_url()
#         instance.save()
