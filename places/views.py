from django.template.loader import get_template
from django.http import HttpResponse
from django.utils import timezone
from events.models import Event
from places.models import Place

# Create your views here.


def place_website(request, place_id):

    place_template = get_template('room.html')

    place_obj = Place.objects.filter(id=place_id)

    this_place_id = place_id

    # filter for event obejts only for requested place,
    # filtered for now and next events
    try:
        place_event_now = Event.objects.get(
            place=place_id,
            start_date_time__lt=timezone.now().strftime('%Y-%m-%d %H:%M:%S'),
            stop_date_time__gt=timezone.now().strftime('%Y-%m-%d %H:%M:%S'),
            )
    except:
        place_event_now = None

    try:
        place_events_next = Event.objects.filter(
            place=place_id,
            start_date_time__date=timezone.now().strftime('%Y-%m-%d'),
            start_date_time__gt=timezone.now().strftime('%Y-%m-%d %H:%M:%S'),
            ).order_by('start_date_time')
    except:
        place_events_next = None

    place_context = {
        'place_obj': place_obj,
        'place_event_now': place_event_now,
        'place_events_next': place_events_next,
        'this_place_id': this_place_id,
    }

    return HttpResponse(place_template.render(place_context))
