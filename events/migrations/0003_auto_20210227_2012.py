# Generated by Django 3.1.7 on 2021-02-27 20:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_event_display_type'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'verbose_name': 'Wydarzenie', 'verbose_name_plural': 'Wydarzenia'},
        ),
    ]
