# Generated by Django 3.0.7 on 2020-09-21 20:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='display_type',
            field=models.CharField(choices=[('T', 'Text'), ('I', 'Image'), ('TI', 'Text+Image')], default='T', max_length=2),
            preserve_default=False,
        ),
    ]
