from django.contrib import admin
from events.models import Event
from datetime import datetime
from django import forms


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = '__all__'

    def clean(self):
        start_date_time = self.cleaned_data.get('start_date_time')
        stop_date_time = self.cleaned_data.get('stop_date_time')
        event_place = self.cleaned_data.get('place')
        events = Event.objects.filter(place=event_place)

        if stop_date_time < start_date_time:
            raise forms.ValidationError(
                "Data końcowa spotkania nie może być mniejsza od startowej!")
            return self.cleaned_data

        for entry in events:
            if ((start_date_time < entry.start_date_time and stop_date_time > entry.start_date_time) or
                ((start_date_time >= entry.start_date_time and start_date_time <= entry.stop_date_time) and
                 (stop_date_time >= entry.start_date_time and stop_date_time <= entry.stop_date_time)) or
                    (start_date_time < entry.stop_date_time and stop_date_time > entry.stop_date_time)):
                raise forms.ValidationError(
                    "Istnieje już spotkanie dla wybranego miejsca w podanym przedziale czasowym!")
                return self.cleaned_data


class EventAdmin(admin.ModelAdmin):
    form = EventForm
    fieldsets = (
        (None, {
            'fields': ('title', 'info', 'image', 'display_type', 'place', 'company', ('start_date_time', 'stop_date_time',),)
        }),
    )
    list_display = ('title', 'place', 'company',
                    'start_date_time', 'stop_date_time', )
    list_filter = ('place', 'company', 'display_type',
                   'start_date_time', 'stop_date_time', )


# Register your models here.
admin.site.register(Event, EventAdmin)
