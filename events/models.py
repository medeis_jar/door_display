from django.db import models
from django.db.models.signals import post_save
from django.utils import timezone
from djqscsv import write_csv

# Create your models here.

DISPLAY_TYPES = (
    ('T', 'Text'),
    ('I', 'Image'),
    ('TI', 'Text+Image'),
)


class Event(models.Model):
    title = models.CharField(max_length=100)
    info = models.CharField(max_length=100, null=True, blank=True)
    image = models.ImageField(
        upload_to='events/%Y/%m/%d', null=True, blank=True)
    display_type = models.CharField(
        max_length=2, null=False, blank=False, choices=DISPLAY_TYPES)
    start_date_time = models.DateTimeField(default=timezone.now)
    stop_date_time = models.DateTimeField(default=timezone.now)
    meeting_confirmed = models.BooleanField(default=False, editable=True)

    place = models.ForeignKey('places.Place', on_delete=models.CASCADE)
    company = models.ForeignKey('companies.Company', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Wydarzenie'
        verbose_name_plural = 'Wydarzenia'

# Display name of object in object list view
    def __str__(self):
        return self.title


def dump_events_to_csv(sender, **kwargs):

    qs = Event.objects.values('title','info','image','display_type','start_date_time','stop_date_time','place__name','company__name')
    with open('foo.csv', 'wb') as csv_file:
        write_csv(qs, csv_file)

    print('post save callback')


post_save.connect(dump_events_to_csv, sender=Event)
