from django.db import models

# Create your models here.


class Company(models.Model):
    name = models.CharField(max_length=100)
    info = models.CharField(max_length=100, blank=True, null=True)
    image = models.ImageField(
        upload_to='companies/%Y/%m/%d', blank=True, null=True)
    contact = models.CharField(max_length=100, blank=True, null=True)
    website = models.URLField(null=True, blank=True)

    class Meta:
        verbose_name = 'Firma'
        verbose_name_plural = 'Firmy'

# Display name of object in object list view
    def __str__(self):
        return self.name
