from django.contrib import AdminSite


class MyAdminSite(AdminSite):
    site_header = 'Monty Python administration'
